FROM gcc:latest AS builder

COPY ./ /scrutiny/

RUN cd /scrutiny && \
    make docker_build=yes monkeypatch=yes

FROM gcc:latest

RUN mkdir /usr/local/include/scrutiny

COPY --from=builder /scrutiny/include/scrutiny/ /usr/local/include/scrutiny/
COPY --from=builder /scrutiny/libscrutiny.so /usr/local/lib/

RUN ldconfig

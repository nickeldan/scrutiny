/**
 * @file test.h
 * @brief Defines the test macros.
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

#include "definitions.h"

/**
 * @brief Gets the group context.
 *
 * @return  The group context.
 */
void *
scrGroupCtx(void) SCR_EXPORT __attribute__((pure));

/**
 * @brief Gets the real pointer to a monkeypatched function.
 *
 * @param func_name     The name of the function that was monkeypatched.
 *
 * @return              The real pointer to the function or NULL if the function wasn't patched.
 */
void *
scrPatchedFunction(const char *func_name) SCR_EXPORT SCR_NONNULL(1);

/**
 * @brief Skips the current test.
 */
void
scrTestSkip(void) SCR_EXPORT SCR_NORETURN;

#define SCR_CONTEXT_DECL   const char *file_name, const char *function_name, unsigned int line_no
#define SCR_CONTEXT_PARAMS __FILE__, __func__, __LINE__

/**
 * @brief Logs a message.
 */
void
_scrLog(SCR_CONTEXT_DECL, const char *format, ...) SCR_EXPORT;
#define scrLog(...) _scrLog(SCR_CONTEXT_PARAMS, __VA_ARGS__)

void
_scrFail(SCR_CONTEXT_DECL, const char *format, ...) SCR_EXPORT SCR_PRINTF(4) SCR_NORETURN;
/**
 * @brief Fails the current test with a message.
 */
#define scrFail(...) _scrFail(SCR_CONTEXT_PARAMS, __VA_ARGS__)

/**
 * @brief Asserts that an expression is true and fails the test if it isn't.
 */
#define scrAssert(expr)                             \
	do {                                            \
		if (!(expr)) {                              \
			scrFail("Assertion failed: %s", #expr); \
		}                                           \
	} while (0)

#define SCR_ASSERT_FUNC(func, type) \
	void _scrAssert##func(SCR_CONTEXT_DECL, type value1, const char *expr1, type value2, const char *expr2)
#define SCR_ASSERT_MACRO(func, expr1, expr2) \
	_scrAssert##func(SCR_CONTEXT_PARAMS, expr1, #expr1, expr2, #expr2)

SCR_ASSERT_FUNC(Eq, intmax_t) SCR_EXPORT;
/**
 * @brief Asserts that two integers are equal.
 */
#define scrAssertEq(expr1, expr2) SCR_ASSERT_MACRO(Eq, expr1, expr2)

SCR_ASSERT_FUNC(Ne, intmax_t) SCR_EXPORT;
/**
 * @brief Asserts that two integers are not equal.
 */
#define scrAssertNe(expr1, expr2) SCR_ASSERT_MACRO(Ne, expr1, expr2)

SCR_ASSERT_FUNC(Lt, intmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one integer is less than another.
 */
#define scrAssertLt(expr1, expr2) SCR_ASSERT_MACRO(Lt, expr1, expr2)

SCR_ASSERT_FUNC(Le, intmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one integer is less than or equal to another.
 */
#define scrAssertLe(expr1, expr2) SCR_ASSERT_MACRO(Le, expr1, expr2)

SCR_ASSERT_FUNC(Gt, intmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one integer is greater than another.
 */
#define scrAssertGt(expr1, expr2) SCR_ASSERT_MACRO(Gt, expr1, expr2)

SCR_ASSERT_FUNC(Ge, intmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one integer is greater than or equal to another.
 */
#define scrAssertGe(expr1, expr2) SCR_ASSERT_MACRO(Ge, expr1, expr2)

SCR_ASSERT_FUNC(UnsignedEq, uintmax_t) SCR_EXPORT;
/**
 * @brief Asserts that two unsigned integers are equal.
 */
#define scrAssertUnsignedEq(expr1, expr2) SCR_ASSERT_MACRO(UnsignedEq, expr1, expr2)

SCR_ASSERT_FUNC(UnsignedNe, uintmax_t) SCR_EXPORT;
/**
 * @brief Asserts that two unsigned integers are not equal.
 */
#define scrAssertUnsignedNe(expr1, expr2) SCR_ASSERT_MACRO(UnsignedNe, expr1, expr2)

SCR_ASSERT_FUNC(UnsignedLt, uintmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one unsigned integer is less than another.
 */
#define scrAssertUnsignedLt(expr1, expr2) SCR_ASSERT_MACRO(UnsignedLt, expr1, expr2)

SCR_ASSERT_FUNC(UnsignedLe, uintmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one unsigned integer is less than or equal to another.
 */
#define scrAssertUnsignedLe(expr1, expr2) SCR_ASSERT_MACRO(UnsignedLe, expr1, expr2)

SCR_ASSERT_FUNC(UnsignedGt, uintmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one unsigned integer is greater than another.
 */
#define scrAssertUnsignedGt(expr1, expr2) SCR_ASSERT_MACRO(UnsignedGt, expr1, expr2)

SCR_ASSERT_FUNC(UnsignedGe, uintmax_t) SCR_EXPORT;
/**
 * @brief Asserts that one unsigned integer is greater than or equal to another.
 */
#define scrAssertUnsignedGe(expr1, expr2) SCR_ASSERT_MACRO(UnsignedGe, expr1, expr2)

SCR_ASSERT_FUNC(FloatEq, long double) SCR_EXPORT;
/**
 * @brief Asserts that two floating-point values are equal.
 */
#define scrAssertFloatEq(expr1, expr2) SCR_ASSERT_MACRO(FloatEq, expr1, expr2)

SCR_ASSERT_FUNC(FloatNe, long double) SCR_EXPORT;
/**
 * @brief Asserts that two floating-point values are not equal.
 */
#define scrAssertFloatNe(expr1, expr2) SCR_ASSERT_MACRO(FloatNe, expr1, expr2)

SCR_ASSERT_FUNC(FloatLt, long double) SCR_EXPORT;
/**
 * @brief Asserts that one floating-point value is less than another.
 */
#define scrAssertFloatLt(expr1, expr2) SCR_ASSERT_MACRO(FloatLt, expr1, expr2)

SCR_ASSERT_FUNC(FloatLe, long double) SCR_EXPORT;
/**
 * @brief Asserts that one floating-point value is less than or equal to another.
 */
#define scrAssertFloatLe(expr1, expr2) SCR_ASSERT_MACRO(FloatLe, expr1, expr2)

SCR_ASSERT_FUNC(FloatGt, long double) SCR_EXPORT;
/**
 * @brief Asserts that one floating-point value is greater than another.
 */
#define scrAssertFloatGt(expr1, expr2) SCR_ASSERT_MACRO(FloatGt, expr1, expr2)

SCR_ASSERT_FUNC(FloatGe, long double) SCR_EXPORT;
/**
 * @brief Asserts that one floating-point value is greater than or equal to another.
 */
#define scrAssertFloatGe(expr1, expr2) SCR_ASSERT_MACRO(FloatGe, expr1, expr2)

SCR_ASSERT_FUNC(PtrEq, const void *) SCR_EXPORT;
/**
 * @brief Asserts that two pointers are equal.
 */
#define scrAssertPtrEq(expr1, expr2) SCR_ASSERT_MACRO(PtrEq, expr1, expr2)

SCR_ASSERT_FUNC(PtrNe, const void *) SCR_EXPORT;
/**
 * @brief Asserts that two pointers are not equal.
 */
#define scrAssertPtrNe(expr1, expr2) SCR_ASSERT_MACRO(PtrNe, expr1, expr2)

SCR_ASSERT_FUNC(StrEq, const char *) SCR_EXPORT;
/**
 * @brief Asserts that two strings are equal.
 */
#define scrAssertStrEq(expr1, expr2) SCR_ASSERT_MACRO(StrEq, expr1, expr2)

SCR_ASSERT_FUNC(StrNe, const char *) SCR_EXPORT;
/**
 * @brief Asserts that two strings are not equal.
 */
#define scrAssertStrNe(expr1, expr2) SCR_ASSERT_MACRO(StrNe, expr1, expr2)

SCR_ASSERT_FUNC(StrBeginsWith, const char *) SCR_EXPORT;
/**
 * @brief Asserts that one string starts with another.
 */
#define scrAssertStrBeginsWith(expr1, expr2) SCR_ASSERT_MACRO(StrBeginsWith, expr1, expr2)

SCR_ASSERT_FUNC(StrNBeginsWith, const char *) SCR_EXPORT;
/**
 * @brief Asserts that one string doesn't start with another.
 */
#define scrAssertStrNBeginsWith(expr1, expr2) SCR_ASSERT_MACRO(StrNBeginsWith, expr1, expr2)

size_t
_scrAssertStrContains(SCR_CONTEXT_DECL, const char *value1, const char *expr1, const char *value2,
					  const char *expr2) SCR_EXPORT;
/**
 * @brief Asserts that one string contains another.
 *
 * @return  If successful, the offset in the first string where the second string begins.
 */
#define scrAssertStrContains(expr1, expr2) \
	_scrAssertStrContains(SCR_CONTEXT_PARAMS, expr1, #expr1, expr2, #expr2)

SCR_ASSERT_FUNC(StrNContains, const char *) SCR_EXPORT;
/**
 * @brief Asserts that one string doesn't contain another.
 */
#define scrAssertStrNContains(expr1, expr2) SCR_ASSERT_MACRO(StrNContains, expr1, expr2)

SCR_ASSERT_FUNC(CharEq, char) SCR_EXPORT;
/**
 * @brief Asserts that two characters are equal.
 */
#define scrAssertCharEq(expr1, expr2) SCR_ASSERT_MACRO(CharEq, expr1, expr2)

SCR_ASSERT_FUNC(CharNe, char) SCR_EXPORT;
/**
 * @brief Asserts that two characters are not equal.
 */
#define scrAssertCharNe(expr1, expr2) SCR_ASSERT_MACRO(CharNe, expr1, expr2)

void
_scrAssertMemEq(SCR_CONTEXT_DECL, const void *ptr1, const char *expr1, const void *ptr2, const char *expr2,
				size_t size) SCR_EXPORT;
/**
 * @brief Asserts that two memory regions are equal.
 */
#define scrAssertMemEq(expr1, expr2, size) \
	_scrAssertMemEq(SCR_CONTEXT_PARAMS, expr1, #expr1, expr2, #expr2, size)

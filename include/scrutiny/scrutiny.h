/**
 * @file scrutiny.h
 * @brief Master include file.
 */

#pragma once

/**
 * @brief Scrutiny's version.
 */
#define SCRUTINY_VERSION "1.1.0"

#include "run.h"
#include "test.h"

#pragma once

#define SCR_EXPORT		 __attribute__((visibility("default")))
#define SCR_NONNULL(...) __attribute__((nonnull(__VA_ARGS__)))
#define SCR_PRINTF(pos)	 __attribute__((format(printf, pos, pos + 1)))
#define SCR_NORETURN	 __attribute__((noreturn))

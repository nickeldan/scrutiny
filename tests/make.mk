TEST_BINARIES := $(patsubst %.c,%,$(wildcard $(TEST_DIR)/test_*.c))
AUX_LIB := $(TEST_DIR)/libaux.so
CURDIR := $(shell pwd)

TEST_DEPENDENCIES := $(TEST_BINARIES)

ifeq ($(monkeypatch),yes)

$(AUX_LIB): $(TEST_DIR)/aux.c
	$(CC) -shared -fPIC $(CFLAGS) $< -o $@

TEST_DEPENDENCIES += $(AUX_LIB)

$(TEST_DIR)/test_monkeypatch: $(TEST_DIR)/test_monkeypatch.c $(TEST_DIR)/common.h $(SCR_SHARED_LIBRARY) $(AUX_LIB)
	$(CC) $(CFLAGS) $(SCR_INCLUDE_FLAGS) $< -Wl,-rpath $(CURDIR) -Wl,-rpath $(CURDIR)/$(TEST_DIR) -L$(CURDIR) -L$(TEST_DIR) -lscrutiny -laux -o $@

endif

.PHONY:
tests: $(TEST_DEPENDENCIES)
	cd $(TEST_DIR)
	$(TEST_DIR)//test_check_include
	$(TEST_DIR)//test_basic
	$(TEST_DIR)//test_ctx
	$(TEST_DIR)//test_fail_fast
	$(TEST_DIR)//test_no_groups
	$(TEST_DIR)//test_no_tests
ifeq ($(monkeypatch),yes)
	$(TEST_DIR)/test_monkeypatch
endif
	$(TEST_DIR)/test_verbose | tee $(TEST_DIR)/output.txt
	grep "This is a log message" $(TEST_DIR)/output.txt
	grep "This test will be skipped" $(TEST_DIR)/output.txt
	grep "This is stdout" $(TEST_DIR)/output.txt
	grep "This is stderr" $(TEST_DIR)/output.txt
	@rm $(TEST_DIR)/output.txt

$(TEST_DIR)/test_%: $(TEST_DIR)/test_%.c $(TEST_DIR)/common.h $(SCR_SHARED_LIBRARY)
	$(CC) $(CFLAGS) $(SCR_INCLUDE_FLAGS) $< -Wl,-rpath $(CURDIR) -L$(CURDIR) -lscrutiny -o $@

.PHONY:
test_clean:
	@rm -f $(TEST_DEPENDENCIES) $(TEST_DIR)/output.txt

CLEAN_TARGETS += test_clean

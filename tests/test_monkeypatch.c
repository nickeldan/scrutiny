#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include <scrutiny/scrutiny.h>

#ifdef SCR_MONKEYPATCH

pid_t
indirectGetPpid(void);

static void *
group_setup(void *global_ctx)
{
	(void)global_ctx;
	return (void *)(intptr_t)getpid();
}

static pid_t
fake_getppid(void)
{
	return 0;
}

static void
test_fake_getppid(void)
{
	scrAssertEq(getppid(), 0);
}

static void
test_true_getppid(void)
{
	pid_t answer;
	pid_t (*true_getppid)(void);

	answer = (intptr_t)scrGroupCtx();
	true_getppid = scrPatchedFunction("getppid");
	scrAssertPtrNe(true_getppid, NULL);
	scrAssertEq(true_getppid(), answer);
}

static void
test_nonpatched_function(void)
{
	scrAssertPtrEq(scrPatchedFunction("malloc"), NULL);
}

static void
test_selective_patch(void)
{
	pid_t answer;

	answer = (intptr_t)scrGroupCtx();
	scrAssertEq(getppid(), answer);
	scrAssertEq(indirectGetPpid(), 0);
}

int
main(int argc, char **argv)
{
	scrGroup group;
	const scrGroupOptions group_options = {.create_fn = group_setup};
	const scrTestOptions options = {.timeout = 1};

	(void)argc;

	printf("\nRunning %s\n\n", argv[0]);

	group = scrGroupCreate(&group_options);
	if (!scrPatchFunction(group, "getppid", NULL, fake_getppid)) {
		return 1;
	}
	scrAddTest(group, "Fake getppid", test_fake_getppid, &options);
	scrAddTest(group, "Get true getppid", test_true_getppid, &options);
	scrAddTest(group, "Nonpatched function", test_nonpatched_function, &options);

	group = scrGroupCreate(&group_options);
	if (!scrPatchFunction(group, "getppid", "libaux", fake_getppid)) {
		return 1;
	}
	scrAddTest(group, "Selective patching", test_selective_patch, &options);

	return scrRun(NULL, NULL);
}

#else  // SCR_MONKEYPATCH

int
main()
{
	return 0;
}

#endif
